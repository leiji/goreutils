goreutils
=========

## Unix utils reimplemented in Go

_Why?_ 

It's interesting and because I can.

_Why gore?_

Lame pun on saxon pronounciation.

_What?_

* [GNU Core Utils](https://en.wikipedia.org/wiki/GNU_Core_Utilities)
* [Unix Programs](https://en.wikipedia.org/wiki/List_of_Unix_programs)

_Somewhat working_

* `cat`
* `echo`
* `wc`
* `du`
* `basename`
* `dirname`
* `true`
* `false`

Written on FreeBSD, so it's possible some parts have to be ported later.

Future plans include putting everything into a single binary like busybox.
