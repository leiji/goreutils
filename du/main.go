package main

/*
	du - estimate file space usage

	du [-a|-s] [-kx] [-H|-L] [file...]

	Output format: "%d %s\n", <size>, <pathname>

	Missing: 
		* -H
		* -L
		* only count files once (2 hard links to 1 file should only be counted once)
			* TODO how is this handled with -s -c?
		* More comments
		* Separate the FreeBSD specific portions.
		* Maybe allow parallel crawling?
*/

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"syscall"
)

var (
	flag_a bool
	flag_s bool
	flag_c bool
	flag_k bool
	flag_x bool
	flag_H bool
	flag_L bool
)

var sum int64 = 0
var blockSize int64 = 512

func init() {

	// TODO Replace the opengroup description with something more... pleasant to read.
	flag.BoolVar(&flag_a, "a", false, "In addition to the default output, report the size of each file not of type directory in the file hierarchy rooted in the specified file. Regardless of the presence of the -a option, non-directories given as file operands shall always be listed.")
	flag.BoolVar(&flag_s, "s", false, "Instead of the default output, report only the total sum for each of the specified files.")
	flag.BoolVar(&flag_c, "c", false, "Print totals (not POSIX)")
	flag.BoolVar(&flag_k, "k", false, "Write the files sizes in units of 1024 bytes, rather than the default 512-byte units.")
	flag.BoolVar(&flag_x, "x", false, "When evaluating file sizes, evaluate only those files that have the same device as the file specified by the file operand.")
	flag.BoolVar(&flag_H, "H", false, "If a symbolic link is specified on the command line, du shall count the size of the file or file hierarchy referenced by the link.")
	flag.BoolVar(&flag_L, "L", false, "If a symbolic link is specified on the command line or encountered during the traversal of a file hierarchy, du shall count the size of the file or file hierarchy referenced by the link.")

}

func main() {
	flag.Parse()
	var targets []string
	// no file argument == use current directory
	if flag.NArg() < 1 {
		wd, _ := os.Getwd()
		targets = append(targets, wd)
	} else {
		targets = flag.Args()
	}
	if flag_a && flag_s {
		fmt.Fprintf(os.Stderr, "%s: Cannot combine -a and -s\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	if flag_k {
		blockSize = 1024
	}
	// this channel is to synchronize the go routines below
	done := make(chan bool)
	for _, v := range targets {
		// check if the initial target is valid
		if _, err := os.Stat(v); err == nil {
			// kick of a new go routine to get a per target scope
			go func() {
				originInfo, _ := os.Stat(v)
				var partsum int64 = 0
				/*filepath.Walk(v, walk)*/
				filepath.Walk(v, func(path string, info os.FileInfo, err error) error {
					var s, oi *syscall.Stat_t
					if flag_x {
						s = info.Sys().(*syscall.Stat_t)
						oi = originInfo.Sys().(*syscall.Stat_t)
					}
					// directory handling
					if info.IsDir() {
						// are we allowed to decend into other devices?
						if flag_x {
							// we aren't. would we when decending into this directory?
							if s.Dev != oi.Dev {
								// we would change the device. so we are skipping this directory
								return filepath.SkipDir
							}
						}
					} else {
						if err != nil {
							fmt.Printf("%s: %s\n", os.Args[0], err)
							return err
						}
						// is a regular file?
						// mask uninteresting bits to 0 and check if the result is still 0 (= regular)
						mode := info.Mode()
						// remove permission bits
						// mode &= os.FileModeDir | os.ModeAppend | os.ModeExclusive | os.ModeTemporary | os.ModeSymlink | os.ModeDevice | os.ModeNamedPipe | os.ModeSocket | os.ModeCharDevice | os.ModeSetiud | os.ModeSetgid | os.ModeSticky 
						// remove suid sgid sticky and permission bits
						mode &= os.ModeDir | os.ModeAppend | os.ModeExclusive | os.ModeTemporary | os.ModeSymlink | os.ModeDevice | os.ModeNamedPipe | os.ModeSocket | os.ModeCharDevice
						// when not asked to show all entries skip the non-regular files where mode != 0
						if !flag_a && mode != 0 {
							return nil // skip this entry
						}

						// calculate the number of blocks
						byteSize := info.Size()                 // this is also reported by stat(1)
						var blocks int64 = byteSize / blockSize // blocksize
						if flag_c {
							sum += blocks
						}
						// only print sum per target?
						if flag_s {
							partsum += blocks
						} else {
							fmt.Printf("%d\t%s\n", blocks, path)
						}
					}
					return nil
				})
				// print sum per target?
				if flag_s {

					fmt.Printf("%d\t%s\n", partsum, v)
				}
				done <- true
			}()
			<-done
		} else {
			fmt.Printf("%s\n", err)
		}
	}
	if flag_c {
		fmt.Printf("%d\t%s\n", sum, "Total")
	}
}
