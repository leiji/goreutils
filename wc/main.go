package main

/*
	small wc(1) implementation, not quite complete as it ignores the LANG and LC_*
	ENV by itself.

	TODO: Does the go runtime honour these?




*/

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"os"
	"unicode"
	"strings"
)

const DEBUG = false

var (
	cFlag bool
	lFlag bool
	mFlag bool
	wFlag bool
)

func init() {
	flag.BoolVar(&cFlag, "c", false, "Print number of bytes.")
	flag.BoolVar(&lFlag, "l", false, "Print number of lines.")
	flag.BoolVar(&mFlag, "m", false, "Print number of characters.")
	flag.BoolVar(&wFlag, "w", false, "Print number of words.")
}

func _pw(w []rune) {
	print("Curword ")
	for i, k := range w {
		print("[ ", i, ": ", k, " ] ")
	}
	print(`'''`)
	for _, k := range w {
		print(string(k))
	}
	print(`'''`)
	print("\n")
}

func wc(fh io.Reader) (int64, int64, int64, int64) {
	buf := bufio.NewReader(fh)
	linecount := int64(0)
	wordcount := int64(0)
	charcount := int64(0)
	bytecount := int64(0)
	r, s, err := buf.ReadRune()
	curword := []rune{}
	wasSpace := true // so if the file begins with a whitespace it wont count it as a word
	for err == nil {
		charcount += 1
		bytecount += int64(s) // size of the rune
		if DEBUG {
			print("wc: ", r, " ", s, "\n")
			curword = append(curword, r)
		}
		if r == 10 {
			linecount += 1
			if !wasSpace {
				wordcount += 1
				if DEBUG {
					_pw(curword)
				}
				curword = []rune{}
			}
			wasSpace = true // is whitespace after all
		} else if unicode.IsSpace(r) && wasSpace == false {
			wordcount += 1
			if DEBUG {
				_pw(curword)
				curword = []rune{}
			}
			wasSpace = true
		} else if !unicode.IsSpace(r) {
			wasSpace = false
		}
		r, s, err = buf.ReadRune()
	}
	if DEBUG {
		print("lc ", linecount, " wc ", wordcount, " cc ", charcount, "\n")
	}
	return linecount, wordcount, charcount, bytecount
}

// output depending on the flags
// output order is line word char file
func out(l, w, c, b int64, src string) {
	var out []string
	var res string
	
	// check if any flags have been set, custom format here we come
	if lFlag || wFlag || mFlag || cFlag {
		if lFlag {
			out = append(out, fmt.Sprintf("%d", l))
		}
		if wFlag {
			out = append(out, fmt.Sprintf("%d", w))
		}
		if mFlag && !cFlag {
			out = append(out, fmt.Sprintf("%d", c))
		} else if cFlag {
			out = append(out, fmt.Sprintf("%d", b))
		}
		res = strings.Join(out, " ")
	} else {
		// default
		res = fmt.Sprintf("%d %d %d", l, w, b)
	}
	if src != "" {
		res += " " + src
	}
	fmt.Println(res)

}

func main() {
	// totals:
	var tlc, twc, tcc, tbc int64 = 0, 0, 0, 0

	flag.Parse()

	if flag.NArg() < 1 {
		fh := os.Stdin
		llc, lwc, lcc, lbc := wc(fh)
	//	fmt.Printf("%d %d %d\n", llc, lwc, lbc)
		out(llc, lwc, lcc, lbc, "")
	} else {
		var llc, lwc, lcc int64 // local line, word and char count
		var lbc int64           // local byte count
		for i := 0; i < flag.NArg(); i++ {
			fh, err := os.Open(flag.Arg(i))
			if err != nil {
				fmt.Fprint(os.Stderr, err, "\n")
				os.Exit(1)
			}
			defer fh.Close()
			llc, lwc, lcc, lbc = wc(fh)
			// add up for total statistics
			tlc += llc
			twc += lwc
			tcc += lcc
			tbc += lbc
			//fmt.Printf("%d %d %d %s\n", llc, lwc, lbc, flag.Arg(i))
			out(llc, lwc, lcc, lbc, flag.Arg(i))
		}
		if flag.NArg() > 1 {
			out(tlc, twc, tcc, tbc, "total")
			//fmt.Printf("%d %d %d %s\n", tlc, twc, tbc, "total")
		}
	}
}
