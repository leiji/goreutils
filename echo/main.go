package main

/* 
	simple echo, originally intended to include the escape sequences suggested in the POSIX description,
	but realised the whole mess this echo flag/escape thing is.

	see: http://www.in-ulm.de/~mascheck/various/echo+printf/

	Keeping it simple: -n -> no new line. thats it.

	may implement -e later. 
*/

import (
	"os"
	"fmt"
)



func main() {
	var start int = 1 // where to start outputting the args
	var nFlag bool = false // omit the final newline?
	var alen int = len(os.Args)
	if alen >= 2 {
		if os.Args[1] == "-n" {
			start = 2
			nFlag = true
		}
	}
	for i := start; i < alen; i += 1 {
		fmt.Print(os.Args[i])
		if i + 1 < alen { // not the last argument
			fmt.Print(" ")
		}
	}
	if !nFlag {
		fmt.Print("\n")
	}
}
