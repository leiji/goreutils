package main

import "os"
import "io"
import "fmt"

func main() {
	if len(os.Args) == 1 { // nothing but argv[0]
		// hell yeah :D
		io.Copy(os.Stdout, os.Stdin)
	} else {
		for i := 1; i < len(os.Args); i++ {
			f := os.Args[i]
			fh, err := os.Open(f)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Error opening %s: %s\n", f, err)
				//continue
				os.Exit(1)
			}
			defer fh.Close()
			_, err = io.Copy(os.Stdout, fh)
			if err != nil {
				fmt.Fprintf(os.Stderr, "An error occured while reading %s: %s\n", f, err)
				os.Exit(1)
			}
		}
	}

}
