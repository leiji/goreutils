package main

import (
	"path"
	"os"
	"fmt"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s <string>\n", os.Args[0])
		os.Exit(1)
	}

	fmt.Println(path.Dir(os.Args[1]))

}
