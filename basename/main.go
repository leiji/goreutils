package main

import (
	"fmt"
	"os"
	"path"
	"strings"
)

func main() {
	var s, suffix string = "", ""
	if len(os.Args) < 2 {
		fmt.Fprintf(os.Stderr, "Usage: %s <string> [<suffix>]\n", os.Args[0])
		os.Exit(1)
	}
	s = os.Args[1]
	if len(os.Args) >= 3 {
		suffix = os.Args[2]
	}

	basename := path.Base(s)
	// remove suffix if there is one and the suffix to remove is not identical with the basename string
	// see opengroup definition of basename, rule #6
	if suffix != "" && strings.HasSuffix(basename, suffix) && basename != suffix {
		end := len(basename) - len(suffix)
		basename = basename[:end]
	}
	fmt.Printf("%s\n", basename)

}
